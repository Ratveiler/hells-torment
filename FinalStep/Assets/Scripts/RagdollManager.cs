﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RagdollManager : MonoBehaviour
{

    public GameObject show;
    public GameObject hide;

    public void Setup()
    {

        RecursiveBoneSearch(transform);
        Destroy(GetComponent<Animator>());
        show.SetActive(true);
        hide.SetActive(false);

    }

    void RecursiveBoneSearch(Transform t)
    {
        foreach (Transform tC in t)
        {
            DoBone(tC);
            RecursiveBoneSearch(tC);
            show.transform.parent = null;
            Destroy(gameObject, 5f);
            Destroy(show, 4f);
        }
    }

    void DoBone(Transform t)
    {
        if (t.GetComponent<Rigidbody>())
        {
            t.GetComponent<Rigidbody>().isKinematic = false;

        }
    }

}
