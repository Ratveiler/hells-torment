﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MainMenu : MonoBehaviour
{
    [SerializeField] private  CanvasGroup optionsMenu;
    public Slider m_musicSlider;
    public Slider m_soundSlider;

    private void Awake()
    {
        GameController.Instance.AudioManager.PlayMusic(true);
        DataStore.LoadOptions();
    }
    private void Start()
    {
        Time.timeScale = 1f;
        
    }

    public void Play()
    {
        SceneManager.LoadScene(1, LoadSceneMode.Single);
        GameController.Instance.AudioManager.PlayMusic(false);
    }
    
    public void Quit()
    {
        Application.Quit();
    }


    public void SetSoundVolume(float volume)
    {
        GameController.Instance.AudioManager.SfxVolume = volume;
        DataStore.SaveOptions();
    }
    public void SetMusicVolume(float volume)
    {
        GameController.Instance.AudioManager.MusicVolume = volume;
        DataStore.SaveOptions();
    }
    public void UpdateOptions()
    {
        m_musicSlider.value = GameController.Instance.AudioManager.MusicVolume;

        m_soundSlider.value = GameController.Instance.AudioManager.SfxVolume;
    }
    public void ShowWindow(CanvasGroup window)
    {
        window.alpha = 1f;

        window.blocksRaycasts = true;

        window.interactable = true;

        DataStore.LoadOptions();
        // GameController.Instance.State = GameState.Pause;
        //  GameController.Instance.AudioManager.PlaySound("door");
    }

    public void HideWindow(CanvasGroup window)
    {
        window.alpha = 0f;

        window.blocksRaycasts = false;

        window.interactable = false;
       // GameController.Instance.State = GameState.Play;
        // GameController.Instance.AudioManager.PlaySound("door");
    }
}
