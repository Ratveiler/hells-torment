﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerScaleHead : MonoBehaviour
{
    [SerializeField] private GameObject Head;
    [SerializeField] private GameObject HeadDoll;
    [SerializeField] private ParticleSystem _particleSystem;
    [SerializeField] private GameObject fakeHead;
    [SerializeField] private Transform spawnFakeHead;
    [SerializeField] private RagdollManager ragdollManager;



   
    private void OnTriggerEnter(Collider other)
    {
        Damager damager = other.gameObject.GetComponent<Damager>();
        if (damager != null || _particleSystem != null || fakeHead != null)
        {
            Head.transform.localScale = new Vector3(0, 0, 0);
            _particleSystem.Play(true); ;
            fakeHead = Instantiate(fakeHead, spawnFakeHead.position, Quaternion.identity);
            fakeHead.GetComponent<Rigidbody>().AddForce(new Vector3(Random.Range(-10.0f, 10.0f), Random.Range(-10.0f, 10.0f), Random.Range(-10.0f, 10.0f)) * 20f);
            HeadDoll.transform.localScale = new Vector3(0, 0, 0);
            Destroy(fakeHead, 3f);
           // ragdollManager.Setup();
            GetComponentInParent<EnemyControl>().Hit(50);
        }
    }
}
