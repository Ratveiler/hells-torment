﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public enum GameState { Play, Pause }
public delegate void UpdateHeroParametersHandler(HeroParameters parameters);

public class GameController : MonoBehaviour
{
    public event UpdateHeroParametersHandler OnUpdateHeroParameters;
    static private GameController _instance;

    [SerializeField] private int hitScore;
    [SerializeField] private int killScore;
    // [SerializeField] private int killExperience;

    private GameState state;
    private PlayerControl player;
    private HeroParameters hero;
    public Audio audioManager;


    public Audio AudioManager { get => audioManager; set => audioManager = value; }
    public HeroParameters Hero { get => hero; set => hero = value; }
    public PlayerControl Player { get => player; set => player = value; }

    private void Awake()
    {
        if (_instance == null)
        {
            _instance = this;
        }
        else
        {
            if (_instance != this)
            {
                Destroy(gameObject);
            }
        }
        DontDestroyOnLoad(gameObject);

        state = GameState.Play;

        InitializeAudioManager();

    }
    private void InitializeAudioManager()
    {
        audioManager.SourceSFX = gameObject.AddComponent<AudioSource>();
        audioManager.SourceMusic = gameObject.AddComponent<AudioSource>();
        audioManager.SourceRandomPitchSFX = gameObject.AddComponent<AudioSource>();
        gameObject.AddComponent<AudioListener>();
        DataStore.LoadOptions();
    }

    public void StartNewLevel()
    {

        if (OnUpdateHeroParameters != null)
        {
            OnUpdateHeroParameters(hero);
        }
        state = GameState.Play;
    }
    public void Hit(IDestructable victim)
    {
        if (victim.GetType() == typeof(EnemyControl))
        {
            ScoreModule.Instance.AddScore(hitScore);
            Debug.Log("Hit Zombie");
        }
        if (victim.GetType() == typeof(PlayerControl))
        {
            HUD.Instance.HealthBar.value = victim.Health;
            Debug.Log("take damage");
        }

    }

    public void Killed(IDestructable victim)
    {
        if (victim.GetType() == typeof(EnemyControl))
        {
            ScoreModule.Instance.AddScore(killScore);
            Debug.Log("Kill Zombie");

        }
        if (victim.GetType() == typeof(PlayerControl))
        {
            GameOver();
        }

    }
    public static GameController Instance
    {
        get
        {
            if (_instance == null)
            {
                GameObject gameController =
                Instantiate(Resources.Load("Prefabs/GameController")) as GameObject;
                _instance = gameController.GetComponent<GameController>();
            }
            return _instance;
        }
    }
    public GameState State
    {
        get
        {
            return state;
        }
        set
        {
            if (value == GameState.Play)
            {
                Time.timeScale = 1.0f;
                Cursor.lockState = CursorLockMode.Locked;
                Cursor.visible = false;
            }
            else
            {
                Time.timeScale = 0.0f;
                Cursor.lockState = CursorLockMode.None;
                Cursor.visible = true;
            }
            state = value;
        }
    }


    public void RestartLevel()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex, LoadSceneMode.Single);
        GameController.Instance.State = GameState.Play;



    }
    public void LoadMainMenu()
    {
        SceneManager.LoadScene("MainMenu", LoadSceneMode.Single);

    }
    public void GameOver()
    {
        HUD.Instance.ShowLevelLoseWindow();

    }
    //private void OnDestroy()
    //{
    //    GameController.Instance.OnUpdateHeroParameters -=
    //    HandleOnUpdateHeroParameters;
    //}
    //public void LevelUp()
    //{
    //    if (OnUpdateHeroParameters != null)
    //    {
    //        OnUpdateHeroParameters(hero);
    //    }

    //}
}