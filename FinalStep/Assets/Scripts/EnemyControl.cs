﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyControl : Creature
{
    
    RagdollManager ragdollManager;
    private GlobalVars globalVars;

    void Start()
    {
      
        ragdollManager = GetComponent<RagdollManager>();
        _animator = GetComponent<Animator>();
        globalVars = GameObject.Find("GlobalVars").GetComponent<GlobalVars>();
        globalVars.MobList.Add(gameObject); //добавляем себя в общий лист мобов
        globalVars.MobCount++; //увеличиваем счетчик мобов
    }
 
    public override void Die()
    {
        
        GameController.Instance.Killed(this);
        ragdollManager.Setup();
        if (globalVars != null)
        {
            globalVars.MobList.Remove(gameObject); //удаляем себя из глобального списка мобов
            globalVars.MobCount--; //уменьшаем глобальный счетчик мобов на 1
        }
    }

}
