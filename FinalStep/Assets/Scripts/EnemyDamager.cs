﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyDamager : MonoBehaviour
{
    [SerializeField] private float damage;

    public float Damage
    {
        get { return damage; }
        set { damage = value; }
    }

    private void OnTriggerEnter(Collider other)
    {
        IDestructable player = other.gameObject.GetComponent<PlayerControl>();
        if (player != null)
        {
            player.Hit(Damage);
            Debug.Log(other.gameObject.name);
        }
    }
}
