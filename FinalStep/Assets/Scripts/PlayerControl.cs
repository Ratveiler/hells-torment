﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerControl : Creature
{
    [SerializeField] private GameObject weaponTriger;
    [SerializeField] private GameObject kickTriger;
  
    void Start()
    {
        GameController.Instance.Player = this;
        GameController.Instance.OnUpdateHeroParameters += HandleOnUpdateHeroParameters;
        _animator = GetComponent<Animator>();
        OnDisableWeapon();
       

    }
    private void HandleOnUpdateHeroParameters(HeroParameters parameters)
    {
        Health = parameters.MaxHealth;
        Damage = parameters.Damage;
        Speed = parameters.Speed;
    }

    void Update()
    {
        if (Input.GetButtonDown("Fire1"))
        {
            _animator.SetTrigger("Attack");
            weaponTriger.SetActive(true);
        }
        if (Input.GetButtonDown("Fire2"))
        {
            _animator.SetTrigger("Attack2");
            weaponTriger.SetActive(true);
        }
        if (Input.GetKeyDown(KeyCode.Q))
        {
            _animator.SetTrigger("Kick");
            kickTriger.SetActive(true);
        }
        if (Input.GetKeyDown(KeyCode.F))
        {
            _animator.SetTrigger("sAttack");
            weaponTriger.SetActive(true);
        }
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            HUD.Instance.ShowGameWindow();
        }
    }
   
    public void OnDisableWeapon()
    {
        weaponTriger.SetActive(false);
        kickTriger.SetActive(false);
    }
    private void OnDestroy()
    {
        GameController.Instance.OnUpdateHeroParameters -=
                        HandleOnUpdateHeroParameters;
    }

}
