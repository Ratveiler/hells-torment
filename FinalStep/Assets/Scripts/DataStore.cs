﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class DataStore
{


    private const string MUSIC_VOLUME_KEY = "MusicVolume";

    private const string SOUND_VOLUME_KEY = "SoundVolume";

    private const int SCORE_DEFAULT = 0;

    private const string BEST_SCORE = "BestScore";

    private const float DEFAULT_VOLUME = 0.75f;


    static public void SaveOptions()
    {

        PlayerPrefs.SetFloat(MUSIC_VOLUME_KEY,
                    GameController.Instance.AudioManager.MusicVolume);

        PlayerPrefs.SetFloat(SOUND_VOLUME_KEY,
            GameController.Instance.AudioManager.SfxVolume);

        PlayerPrefs.Save();
    }
    static public void LoadOptions()
    {
        GameController.Instance.AudioManager.MusicVolume =
            PlayerPrefs.GetFloat(MUSIC_VOLUME_KEY, DEFAULT_VOLUME);
        GameController.Instance.AudioManager.SfxVolume =
            PlayerPrefs.GetFloat(SOUND_VOLUME_KEY, DEFAULT_VOLUME);
    }
    static public void LoadGame()
    {

        ScoreModule.Instance.BestScore =
             PlayerPrefs.GetInt(BEST_SCORE,SCORE_DEFAULT);


    }

    static public void SaveGame()
    {




        PlayerPrefs.SetInt(BEST_SCORE, ScoreModule.Instance.BestScore);
        PlayerPrefs.Save();



    }


}
