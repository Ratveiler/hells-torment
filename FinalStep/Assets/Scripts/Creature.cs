﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Creature : MonoBehaviour, IDestructable
{
    protected Animator _animator;
    protected Rigidbody2D _rigidbody;
    [SerializeField] private float speed = 3;
    [SerializeField] private float damage = 25;
    [SerializeField] private float health = 100;


    protected float Damage { get => damage; set => damage = value; }
    protected float Speed { get => speed; set => speed = value; }
    public float Health
    {
        get
        {
            return health;
        }
        set
        {
            health = value;
        }
    }


    void Awake()
    {
       _animator = gameObject.GetComponentInChildren<Animator>();
        _rigidbody = gameObject.GetComponent<Rigidbody2D>();
    }

    public virtual void Die()
    {
        GameController.Instance.Killed(this);
    }



    public void Hit(float damage)
    {
        GameController.Instance.Hit(this);
        Health -= damage;
        _animator.SetTrigger("Hit");
        

        if (Health <= 0)
        {
            Die();
        }
    }


}


