﻿using UnityEngine;
using UnityEngine.UI;

public class ScoreModule : MonoBehaviour
{
    static private ScoreModule instance;

    [SerializeField] private int score, bestScore;
    [SerializeField] private Text[] m_scoreValue;
    [SerializeField] private Text m_bestScoreValue;

    public static ScoreModule Instance
    {
        get
        {
            return instance;
        }
    }

    public int Score { get => score; set => score = value; }
    public int BestScore { get => bestScore; set => bestScore = value; }

    private void Awake()
    {
        instance = this;
        DataStore.LoadGame();
       // UpdateBestScore();
        m_bestScoreValue.text = BestScore.ToString();
    }

    public void AddScore(int value)
    {
        Score += value;
        UpdateBestScore();
        for (int i = 0; i < m_scoreValue.Length; i++)
        {
            m_scoreValue[i].text = Score.ToString();
        }
    }
    public void UpdateBestScore()
    {
        if (Score > BestScore)
        {
            BestScore = Score;
            DataStore.SaveGame();

            m_bestScoreValue.text = BestScore.ToString();

        }
    }
}
