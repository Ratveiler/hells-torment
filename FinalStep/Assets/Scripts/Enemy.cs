﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Enemy : MonoBehaviour
{
    
    [SerializeField] private GameObject target;
    private NavMeshAgent agent;
    private Animator animator;
    //private bool seeTarget;
  

    void Awake()
    {

        agent = GetComponent<NavMeshAgent>();
        animator = GetComponent<Animator>();


    }
    private void Start()
    {
        target = GameObject.FindGameObjectWithTag("Player");
    }

    void Update()
    {
        agent.SetDestination(target.transform.position);
        CheckTargetVisibility();
        float distance = Vector3.Distance(target.transform.position, transform.position);
        if (agent.speed <= 0.1f)
        {
            animator.SetBool("Run", false);
        }

        if (agent.speed >= 0.1f)
        {


            animator.SetBool("Run", true);
           // agent.SetDestination(target.position);
            if (distance <= agent.stoppingDistance)
            {

                FaceTarget();

            }
        }
        if (distance <= 1.5f)
        {
            agent.speed = 0f;
            animator.SetBool("Run", false);
            animator.SetBool("Attack", true);

        }
        else
        {
            animator.SetBool("Attack", false);
            agent.speed = 3.5f;
        }
    }

    private void CheckTargetVisibility()
    {
        Vector3 targetDirection = target.transform.position - transform.position;

        Ray ray = new Ray(transform.position, targetDirection);

        RaycastHit hit;
        if (Physics.Raycast(ray, out hit))
        {
            if (hit.transform == target)
            {
               // seeTarget = true;
                return;
            }
        }
       // seeTarget = false;
    }

    void FaceTarget()
    {
        Vector3 direction = (target.transform.position - transform.position).normalized;
        Quaternion lookRotation = Quaternion.LookRotation(new Vector3(direction.x, 0, direction.z));
        transform.rotation = Quaternion.Slerp(transform.rotation, lookRotation, Time.deltaTime * 5f);
    }

}
