﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Damager : MonoBehaviour
{
    [SerializeField] private float damage;
    
    public float Damage
    {
        get { return damage; }
        set { damage = value; }
    }

    private void OnTriggerEnter(Collider other)
    {
        IDestructable enemy = other.gameObject.GetComponent<EnemyControl>();
        if (enemy != null)
        {
            enemy.Hit(Damage);
            Debug.Log(other.gameObject.name);
        }
    }
    

}
