﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HUD : MonoBehaviour
{
    [SerializeField] private Slider m_musicSlider;
    [SerializeField] private Slider m_soundSlider;
    [SerializeField] private CanvasGroup loseMenu;
    [SerializeField] private CanvasGroup gameMenu;
    static private HUD _instance;
    public Slider HealthBar;


    public static HUD Instance
    {
        get
        {
            return _instance;
        }
    }

   // public Slider HealthBar { get => HealthBar; set => HealthBar = value; }

    private void Awake()
    {
        _instance = this;

    }
    private void Start()
    {
        GameController.Instance.StartNewLevel();
        GameController.Instance.OnUpdateHeroParameters += HandleOnUpdateHeroParameters;
    }
    public void HandleOnUpdateHeroParameters(HeroParameters parameters)
    {
        HealthBar.maxValue = parameters.MaxHealth;
        HealthBar.value = parameters.MaxHealth;
    }

    //public void UpdateHealth(PlayerControl parameters)
    //{
    //    healthBar.maxValue = parameters.MaxHealth;
    //    healthBar.value = parameters.Health;
    //}
    public void ShowWindow(CanvasGroup window)
    {
        window.alpha = 1f;

        window.blocksRaycasts = true;

        window.interactable = true;
        GameController.Instance.State = GameState.Pause;
    }

    public void HideWindow(CanvasGroup window)
    {
        window.alpha = 0f;

        window.blocksRaycasts = false;

        window.interactable = false;
        GameController.Instance.State = GameState.Play;
    }

    public void ButtonRestart()
    {
        GameController.Instance.RestartLevel();
    }

    public void ButtonMainMenu()
    {
        GameController.Instance.LoadMainMenu();
    }

    public void ShowLevelLoseWindow()
    {
        ShowWindow(loseMenu);
        GameController.Instance.State = GameState.Pause;
    }
    public void ShowGameWindow()
    {
        ShowWindow(gameMenu);
        GameController.Instance.State = GameState.Pause;
    }

    public void SetSoundVolume(float volume)
    {
        GameController.Instance.AudioManager.SfxVolume = volume;
        DataStore.SaveOptions();
    }
    public void SetMusicVolume(float volume)
    {
        GameController.Instance.AudioManager.MusicVolume = volume;
        DataStore.SaveOptions();
    }
    public void UpdateOptions()
    {
        m_musicSlider.value = GameController.Instance.AudioManager.MusicVolume;

        m_soundSlider.value = GameController.Instance.AudioManager.SfxVolume;
    }
    private void OnDestroy()
    {
        GameController.Instance.OnUpdateHeroParameters -=
        HandleOnUpdateHeroParameters;
    }
}
