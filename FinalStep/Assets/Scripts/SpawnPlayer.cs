﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnPlayer : MonoBehaviour
{
    [SerializeField] private GameObject player;
    [SerializeField] private Transform spawnPoint;
  void Start()
    {
        Instantiate(player, spawnPoint.position, Quaternion.identity);
    }

    
}
