﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerScaleHand : MonoBehaviour
{
    [SerializeField] private GameObject Hand;
    [SerializeField] private GameObject HandDoll;
    [SerializeField] private ParticleSystem _particleSystem;
    [SerializeField] private GameObject fakeHand;
    [SerializeField] private Transform spawnFakeHand;
    private SphereCollider sphereCollider;



    private void Start()
    {
        sphereCollider = GetComponent<SphereCollider>();
    }
    private void OnTriggerEnter(Collider other)
    {
        Damager damager = other.gameObject.GetComponent<Damager>();
        if (damager != null || _particleSystem != null || fakeHand != null)
        {
            Hand.transform.localScale = new Vector3(0, 0, 0);
            _particleSystem.Play(true); ;
            fakeHand = Instantiate(fakeHand, spawnFakeHand.position, Quaternion.identity);
            fakeHand.GetComponent<Rigidbody>().AddForce(new Vector3(Random.Range(-10.0f, 10.0f), 0, Random.Range(-10.0f, 10.0f)) * 10f);
            sphereCollider.radius = 0;
            HandDoll.transform.localScale = new Vector3(0, 0, 0);
            Destroy(sphereCollider);
            Destroy(fakeHand, 3f);
            GetComponentInParent<EnemyControl>().Hit(25);
            
        }
        
    }

}

